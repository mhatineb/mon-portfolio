
window.onload = function () {
    document.getElementById("languageImage").src = "img/uk.png";
    document.getElementById("languageImage-lg").src = "img/uk.png";
    updateTextToFrench(); // Met le texte en français au chargement
}

//Language
function updateTextToEnglish() {
    // Mise à jour du contenu texte en anglais
    document.getElementById('description').innerHTML = `
        I am nearing the end of my apprenticeship to obtain the Level 6 RNCP Web Designer and Developer certification (equivalent to BAC+3/4) with La Fabrique Simplon.
Passionate about mobile development with Flutter and DevOps culture, I am available to explore new professional challenges starting in April 2025.
    `;
    document.getElementById('boutoncv').innerHTML = 'DOWNLOAD MY RESUME';

    // Utilisation correcte des sélecteurs pour les titres de sections
    document.querySelector('#ancrecompetences h2').innerHTML = "My Skills";
    document.querySelector('#ancreprojets h2').innerHTML = "My Projects";

    // Mise à jour du menu avec un tableau
    const menuItems = [
        "About Me",
        "My Skills",
        "My Projects"
    ];

    const menuLinks = document.querySelectorAll('#menuNav li a');
    menuLinks.forEach((link, index) => {
        link.innerHTML = menuItems[index];
    });
}

function updateTextToFrench() {
    // Mise à jour du contenu texte en français
    document.getElementById('description').innerHTML = `
        Je suis en fin d'alternance pour obtenir la certification Conceptrice et Développeuse web-niveau 6 RNCP (BAC+3/4) avec la fabrique Simplon.<br>
        Passionnée par le dev mobile avec Flutter et la culture DevOps, je suis disponible pour explorer de nouveaux défis professionnels à partir d'Avril 2025.
    `;
    document.getElementById('boutoncv').innerHTML = 'TELECHARGER MON CV';

    // Utilisation correcte des sélecteurs pour les titres de sections
    document.querySelector('#ancrecompetences h2').innerHTML = "Mes Compétences";
    document.querySelector('#ancreprojets h2').innerHTML = "Mes Projets";

    // Mise à jour du menu avec un tableau
    const menuItems = [
        "À propos de moi",
        "Mes compétences",
        "Mes projets"
    ];

    const menuLinks = document.querySelectorAll('#menuNav li a');
    menuLinks.forEach((link, index) => {
        link.innerHTML = menuItems[index];
    });
}

// Gestion du basculement de langue via les boutons toggle
document.getElementById("languageToggle").addEventListener('change', function () {
    if (this.checked) {
        document.getElementById("languageImage").src = "img/france.png";
        updateTextToEnglish();
    } else {
        document.getElementById("languageImage").src = "img/uk.png";
        updateTextToFrench();
    }
});

document.getElementById("languageToggle-lg").addEventListener('change', function () {
    if (this.checked) {
        document.getElementById("languageImage-lg").src = "img/france.png";
        updateTextToEnglish();
    } else {
        document.getElementById("languageImage-lg").src = "img/uk.png";
        updateTextToFrench();
    }
});

function toggleImage(imageId, src1, src2) {
    const imageElement = document.getElementById(imageId);
    if (imageElement) {
        imageElement.src = imageElement.src.endsWith(src1) ? src2 : src1;
    }
}

